
#include <stdbool.h>            //BIBLIOTECA BASICA DE OPERACIONES BOOLEANAS
#include <stdint.h>             //BIBLIOTECA ESTANDAR DE TIPO DE DATOS
#include <inc/tm4c1294ncpdt.h>  //DEFINE EL NOMBRE DE LAS ETIQUETAS DE LOS PUERTOS, SUS DIRECCIONES Y NUMERO DE BITS

void interruptFunction(void);

uint32_t i;     //DECLARAMOS UNA VARIBLE QUE UTILIZAREMOS PARA HACER UN RETARDO DE 3 CICLOS DE RELOJ
uint32_t ctr;   //DECLARAMOS VARIABLE CONTADOR

void interruptFunction(void){
    //SE HA PUESTO PJ1 EN BAJO. HEMOS APLASTADO PSH1.
    ctr = 0;    //INICIALIZAMOS CONTADOR A CERO.

    do{
        SysCtlDelay(53333);      //RETARDO DE 10[ms]
        ctr++;
    }while( ( (GPIO_PORTJ_AHB_DATA_R & 0x02) == 0x00) & (ctr < 1000) );

    //------------------------------ SECUENCIA TRIGGER DE LED 1 ----------------------------------
    GPIO_PORTN_DATA_R |= 0x02;      //ENCENDEMOS EL LED 1 CORRESPONDIENTE AL PN1
    SysCtlDelay(5333333);           //RETARDO DE 1 SEGUNDO (SysCtlDelay(1) => 187.5[ns])
    GPIO_PORTN_DATA_R &= ~0x02;     //APAGAMOS EL LED1

    //LIMPIAMOS LA BANDERA DE IMTERRUPCION
    GPIO_PORTJ_AHB_ICR_R = 0x02;

}

int main(void){
    //---------------------------------- DEFINICION DE PUERTOS -----------------------------------
    SYSCTL_RCGCGPIO_R = 0x1100;     //HABILITAMOS LOS PUERTOS J Y N
    i = SYSCTL_RCGCGPIO_R;          //RETARDO DE 3 CICLOS DE RELOJ = 3*(1/16M) = 0.187us

    //----------------------------- CONFIGURACION DE LOS PUERTOS J Y N ---------------------------
    GPIO_PORTN_DIR_R = 0x03;        //DECLARAMOS LOS BITS 0 Y 1 DEL PUERTO N COMO SALIDAS
    GPIO_PORTN_DEN_R = 0x03;        //DEFINIMOS LOS BITS 0 Y 1 DEL PUERTO N COMO DIGITALES

    GPIO_PORTJ_AHB_DIR_R = 0x00;    //DECLARAMOS LOS BITS 0 Y 1 DEL PUERTO J COMO ENTRADAS
    GPIO_PORTJ_AHB_DEN_R = 0x02;    //DEFINIMOS EL BIT 1 DEL PUERTO J COMO DIGITAL
    GPIO_PORTJ_AHB_PUR_R = 0x02;    //RESISTENCIA PULL UP PARA PJ1

    //------------------------------ HABILITACION DE INTERRUPCIONES ------------------------------
    GPIO_PORTJ_AHB_IM_R |= 0x02;    //PUERTO J HABILITADO PARA INTERRUPCION
    GPIO_PORTJ_AHB_IS_R |= 0x00;    //DETECCION DE INTERRUPCION POR FLANCO
    GPIO_PORTJ_AHB_IEV_R |= 0x00;   //DE BAJADA
    GPIO_PORTJ_AHB_IBE_R |= 0x00;   //UTILIZAR CONFIGURACION DE _IVE_R
    NVIC_EN1_R |= 0x00080000;       //SE HABILITA EL PUERTO J PARA INTERRUPCIONES EN EL NVIC

    //------------------- APAGAMOS AMBOS LEDs DEL PUERTO N (PN0 Y PN1) --------------------------
    GPIO_PORTN_DATA_R = 0x00;        //APAGAMOS LOS LEDs CORRESPONDIENTES A PORTN0 Y PORTN1

    //------------------------------ SECUENCIA TRIGGER DE LED 1 ----------------------------------
    GPIO_PORTN_DATA_R |= 0x02;       //ENCENDEMOS EL LED 1 CORRESPONDIENTE AL PN1
    SysCtlDelay(5333333);           //RETARDO DE 1 SEGUNDO (SysCtlDelay(1) => 187.5[ns])
    GPIO_PORTN_DATA_R &= ~0x02;      //APAGAMOS EL LED1

    //------------------------------------- LOOP PRINCIPAL ---------------------------------------
    while(1){
        GPIO_PORTN_DATA_R ^=0x01;   //APAGAMOS EL LED0 SI ESTA ENCENDIDO Y LO ENCENDEMOS SI ESTA APAGADO.
        SysCtlDelay(1333333);       //DELAY DE 250[ms]
    }
}
